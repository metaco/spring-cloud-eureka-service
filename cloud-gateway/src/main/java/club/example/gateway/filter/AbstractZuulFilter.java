package club.example.gateway.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.springframework.http.MediaType;

public abstract class AbstractZuulFilter extends ZuulFilter  {

    // 用于过滤器之间传递消息，每个请求的数据保存在每个请求的 ThreadLocal中
    protected RequestContext requestContext;

    // 标识 当前过滤器操作是否需要下个过滤器继续进行过滤
    private final static String NEXT_FILTER = "next";

    @Override
    public boolean shouldFilter() {
        RequestContext currentContext = RequestContext.getCurrentContext();
        return (boolean) currentContext.getOrDefault(NEXT_FILTER, true);
    }

    @Override
    public Object run() throws ZuulException {
        requestContext = RequestContext.getCurrentContext();
        return innerRun();
    }

    protected abstract Object innerRun();

    protected void fail(int code, String message) {
        requestContext.set(NEXT_FILTER, false);
        requestContext.setSendZuulResponse(false);

        requestContext.getResponse().setContentType(MediaType.APPLICATION_JSON_VALUE);

        requestContext.setResponseStatusCode(code);
        requestContext.setResponseBody(message + "-_-!");
    }

    protected void success() {
        requestContext.set(NEXT_FILTER, true);
    }
}