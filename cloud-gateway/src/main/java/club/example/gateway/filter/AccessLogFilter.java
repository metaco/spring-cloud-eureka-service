package club.example.gateway.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@Component
public class AccessLogFilter extends AbstractPostZuulFilter {

    @Override
    protected Object innerRun() {
        HttpServletRequest request = requestContext.getRequest();
        // 读取startTime 请求的时间戳
        Long startTime = (Long) requestContext.get("startTime");
        if (null != startTime) {
            String requestURI = request.getRequestURI();
            long duration = System.currentTimeMillis() - startTime;
            // 打印日志
            // 从网关通过的请求，打印持续时间
            log.info("request uri : {} : duration : {}", requestURI, duration);
            success();
        }
        return null;
    }

    @Override
    public int filterOrder() {
        return FilterConstants.SEND_RESPONSE_FILTER_ORDER - 1;
    }
}
