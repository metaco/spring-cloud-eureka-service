package club.example.gateway.filter;

import com.google.common.util.concurrent.RateLimiter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * 限流过滤器
 */
@Slf4j
@Component
public class RateLimiterFilter extends AbstractPreZuulFilter {

    /**
     * 每秒可以获取到两个令牌
     */
    private RateLimiter rateLimiter = RateLimiter.create(2.0);

    @Override
    protected Object innerRun() {
        HttpServletRequest contextRequest = requestContext.getRequest();
        if (rateLimiter.tryAcquire()) {
            log.info("rate token success...");
            success();
            return null;
        } else {
            log.error("rate limit : {}", contextRequest.getRequestURI());
            fail(HttpStatus.TOO_MANY_REQUESTS.value(), "too many request");
            return null;
        }
    }

    @Override
    public int filterOrder() {
        return 2;
    }
}
