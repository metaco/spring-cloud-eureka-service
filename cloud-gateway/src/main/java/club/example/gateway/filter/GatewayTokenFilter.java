package club.example.gateway.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@Component
public class GatewayTokenFilter extends AbstractPreZuulFilter {

    @Override
    protected Object innerRun() {
        HttpServletRequest contextRequest =
                requestContext.getRequest();
        log.info(String.format("%s request to %s",
                contextRequest.getMethod(), contextRequest.getRequestURL().toString()));

        String accessToken = contextRequest.getParameter("access_token");
        if (! StringUtils.hasLength(accessToken)) {
            log.error("error# token is empty");
            fail(HttpStatus.UNAUTHORIZED.value(), "error# token is empty");
            return null;
        }
        success();
        return null;
    }

    @Override
    public int filterOrder() {
        return 1;
    }
}
