package club.example.gateway.filter;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 存储客户端发起请求的时间戳
 *
 */

@Slf4j
@Component
public class PreRequestFilter extends AbstractPreZuulFilter {

    @Override
    protected Object innerRun() {
        requestContext.set("startTime", System.currentTimeMillis());
        success();
        return null;
    }

    @Override
    public int filterOrder() {
        return 0;
    }
}
