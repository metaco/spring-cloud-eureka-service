package club.example.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;


/**
 *  当前网关服务, 也是 Eureka Client,
 *  SpringCloudApplication 注解组合了：springBoot 应用注解 + 服务发现注解 + 熔断注解
 */
@EnableZuulProxy
@SpringCloudApplication
public class CloudGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(CloudGatewayApplication.class, args);
    }
}
