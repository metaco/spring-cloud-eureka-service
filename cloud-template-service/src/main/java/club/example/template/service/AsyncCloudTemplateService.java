package club.example.template.service;

import club.example.template.entity.CloudTemplate;

public interface AsyncCloudTemplateService {

    /**
     * 根据模板异步的创建优惠券码
     * @param cloudTemplate CloudTemplate
     */
    void buildByCloudTemplate(CloudTemplate cloudTemplate);
}
