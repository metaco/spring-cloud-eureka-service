package club.example.template.service;

import club.example.template.entity.CloudTemplate;
import club.example.template.request.CloudTemplateRequest;

public interface CloudTemplateService {

    /**
     * 构建Cloud模板
     * @param cloudTemplateRequest CloudTemplateRequest
     * @return CloudTemplate
     */
    CloudTemplate build(CloudTemplateRequest cloudTemplateRequest);


}
