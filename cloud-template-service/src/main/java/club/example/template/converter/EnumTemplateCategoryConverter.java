package club.example.template.converter;

import club.example.cloudservice.constant.EnumTemplateCategory;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;


@Converter
public class EnumTemplateCategoryConverter implements AttributeConverter<EnumTemplateCategory, String> {

    @Override
    public String convertToDatabaseColumn(EnumTemplateCategory attribute) {
        return attribute.getCode();
    }

    @Override
    public EnumTemplateCategory convertToEntityAttribute(String dbData) {
        return EnumTemplateCategory.of(dbData);
    }
}
