package club.example.template.converter;

import club.example.cloudservice.view.TemplateRule;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.io.IOException;

@Slf4j
@Converter
public class TemplateRuleConverter implements AttributeConverter<TemplateRule, String> {

    private final static ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public String convertToDatabaseColumn(TemplateRule attribute) {
        try {
            return objectMapper.writeValueAsString(attribute);
        } catch (JsonProcessingException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public TemplateRule convertToEntityAttribute(String dbData) {
        try {
            return objectMapper.readValue(dbData, TemplateRule.class);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
