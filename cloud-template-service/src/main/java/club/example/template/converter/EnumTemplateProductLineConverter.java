package club.example.template.converter;

import club.example.cloudservice.constant.EnumTemplateProductLine;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * <h1>优惠券分类枚举属性转换器</h1>
 * AttributeConverter<X, Y>
 * X: 是实体属性的类型
 * Y: 是数据库字段的类型
 */

@Converter
public class EnumTemplateProductLineConverter implements AttributeConverter<EnumTemplateProductLine, Integer> {

    @Override
    public Integer convertToDatabaseColumn(EnumTemplateProductLine attribute) {
        return attribute.getCode();
    }

    @Override
    public EnumTemplateProductLine convertToEntityAttribute(Integer dbData) {
        return EnumTemplateProductLine.of(dbData);
    }
}
