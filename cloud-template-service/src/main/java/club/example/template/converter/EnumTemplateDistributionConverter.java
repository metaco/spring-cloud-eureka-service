package club.example.template.converter;

import club.example.cloudservice.constant.EnumDistributionTarget;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class EnumTemplateDistributionConverter implements AttributeConverter<EnumDistributionTarget, Integer> {

    @Override
    public Integer convertToDatabaseColumn(EnumDistributionTarget attribute) {
        return attribute.getCode();
    }

    @Override
    public EnumDistributionTarget convertToEntityAttribute(Integer dbData) {
        return EnumDistributionTarget.of(dbData);
    }
}
