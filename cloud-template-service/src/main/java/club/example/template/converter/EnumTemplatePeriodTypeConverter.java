package club.example.template.converter;

import club.example.cloudservice.constant.EnumTemplatePeriodType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class EnumTemplatePeriodTypeConverter implements AttributeConverter<EnumTemplatePeriodType, Integer> {

    @Override
    public Integer convertToDatabaseColumn(EnumTemplatePeriodType attribute) {
        return attribute.getCode();
    }

    @Override
    public EnumTemplatePeriodType convertToEntityAttribute(Integer dbData) {
        return EnumTemplatePeriodType.of(dbData);
    }
}
