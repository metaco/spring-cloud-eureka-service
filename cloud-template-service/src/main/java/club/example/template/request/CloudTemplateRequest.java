package club.example.template.request;

import club.example.cloudservice.constant.EnumDistributionTarget;
import club.example.cloudservice.constant.EnumTemplateCategory;
import club.example.cloudservice.constant.EnumTemplateProductLine;
import club.example.cloudservice.view.TemplateRule;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

/**
 * 优惠券模板创建请求对象
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CloudTemplateRequest {

    /**
     * 优惠券名称
     */
    private String name;

    /**
     * 优惠券 logo
     */
    private String logo;

    /**
     * 优惠券描述
     */
    private String desc;

    /**
     * 优惠券分类
     */
    private String category;

    /**
     * 产品线
     */
    private Integer productLine;

    /**
     * 总数
     */
    private Integer count;

    /**
     * 创建用户
     */
    private Long userId;

    /**
     * 目标用户
     */
    private Integer target;

    /**
     * 优惠券规则
     */
    private TemplateRule rule;

    /**
     * 校验对象的合法性
     */
    public boolean validate() {

        boolean stringValid = StringUtils.isNotEmpty(name)
                && StringUtils.isNotEmpty(logo)
                && StringUtils.isNotEmpty(desc);
        boolean enumValid = null != EnumTemplateCategory.of(category)
                && null != EnumTemplateProductLine.of(productLine)
                && null != EnumDistributionTarget.of(target);
        boolean numValid = count > 0 && userId > 0;

        return stringValid && enumValid && numValid && rule.validate();
    }
}