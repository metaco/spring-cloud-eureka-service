package club.example.template.entity;

import club.example.cloudservice.constant.EnumDistributionTarget;
import club.example.cloudservice.constant.EnumTemplateCategory;
import club.example.cloudservice.constant.EnumTemplateProductLine;
import club.example.cloudservice.view.TemplateRule;
import club.example.template.converter.EnumTemplateCategoryConverter;
import club.example.template.converter.EnumTemplateDistributionConverter;
import club.example.template.converter.EnumTemplateProductLineConverter;
import club.example.template.converter.TemplateRuleConverter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "coupon_template")
public class CloudTemplate {
    /** 自增主键 */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    /** 是否是可用状态 */
    @Column(name = "available", nullable = false)
    private Boolean available;

    /** 是否过期 */
    @Column(name = "expired", nullable = false)
    private Boolean expired;

    /** 优惠券名称 */
    @Column(name = "name", nullable = false)
    private String name;

    /** 优惠券 logo */
    @Column(name = "logo", nullable = false)
    private String logo;

    /** 优惠券描述 */
    @Column(name = "intro", nullable = false)
    private String desc;

    /** 优惠券分类 */
    @Column(name = "category", nullable = false)
    @Convert(converter = EnumTemplateCategoryConverter.class)
    private EnumTemplateCategory category;

    /** 产品线 */
    @Column(name = "product_line", nullable = false)
    @Convert(converter = EnumTemplateProductLineConverter.class)
    private EnumTemplateProductLine productLine;

    /** 总数 */
    @Column(name = "coupon_count", nullable = false)
    private Integer count;

    /** 创建时间 */
    @CreatedDate
    @Column(name = "create_time", nullable = false)
    private Date createTime;

    /** 创建用户 */
    @Column(name = "user_id", nullable = false)
    private Long userId;

    /** 优惠券模板的编码 */
    @Column(name = "template_key", nullable = false)
    private String key;

    /** 目标用户 */
    @Column(name = "target", nullable = false)
    @Convert(converter = EnumTemplateDistributionConverter.class)
    private EnumDistributionTarget target;

    /** 优惠券规则 */
    @Column(name = "rule", nullable = false)
    @Convert(converter = TemplateRuleConverter.class)
    private TemplateRule rule;

    public CloudTemplate(String name, String logo,
                         String desc, String category,
                         int productLine, Integer count, Long userId, String key,
                         int target, TemplateRule rule) {
        this.available = false;
        this.expired = false;
        this.name = name;
        this.logo = logo;
        this.desc = desc;
        this.category = EnumTemplateCategory.of(category);
        this.productLine = EnumTemplateProductLine.of(productLine);
        this.count = count;
        this.userId = userId;
        // 优惠券模板唯一编码 = 4(产品线和类型) + 8(日期: 20190101) + id(扩充为4位)
        this.key = productLine + category +
                new SimpleDateFormat("yyyyMMdd").format(new Date());
        this.target = EnumDistributionTarget.of(target);
        this.rule = rule;
    }
}
