package club.example.template.config;

import club.example.template.config.task.AsyncTaskProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.Assert;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 异步任务执行器，线程池配置
 */
@Slf4j
@EnableAsync
@Configuration
@EnableConfigurationProperties(value = AsyncTaskProperties.class)
public class AsyncPoolingConfig implements AsyncConfigurer {

    private final AsyncTaskProperties taskProperties;

    public AsyncPoolingConfig(AsyncTaskProperties taskProperties) {
        Assert.notNull(taskProperties, "taskProperties could not be null");
        this.taskProperties = taskProperties;
    }

    @Bean
    @Override
    public Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setCorePoolSize(taskProperties.getCorePoolSize());
        threadPoolTaskExecutor.setMaxPoolSize(taskProperties.getMaxPoolSize());
        threadPoolTaskExecutor.setKeepAliveSeconds(taskProperties.getKeepAliveSeconds());
        threadPoolTaskExecutor.setQueueCapacity(taskProperties.getQueueCapacity());
        threadPoolTaskExecutor.setWaitForTasksToCompleteOnShutdown(true);
        threadPoolTaskExecutor.setAwaitTerminationSeconds(60);
        threadPoolTaskExecutor.setThreadNamePrefix(taskProperties.getThreadNamePrefix());
        threadPoolTaskExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());

        threadPoolTaskExecutor.initialize();
        return threadPoolTaskExecutor;
    }

    /**
     * 异步任务执行异常捕获
     * @return Async
     */
    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return new AsyncExceptionHandler();
    }

    class AsyncExceptionHandler implements AsyncUncaughtExceptionHandler {

        /**
         * @param throwable Throwable
         * @param method 异步任务执行的方法名
         * @param objects 参数数组
         */
        @Override
        public void handleUncaughtException(Throwable throwable, Method method, Object... objects) {
            throwable.printStackTrace();
            log.error("异步任务执行错误 # {} # {} # {}",
                    throwable.getMessage(), method.getName(), Arrays.asList(objects).toString() );

            // TODO：发送错误通知
        }
    }
}
