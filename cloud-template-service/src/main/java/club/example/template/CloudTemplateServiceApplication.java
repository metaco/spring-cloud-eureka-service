package club.example.template;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class CloudTemplateServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(CloudTemplateServiceApplication.class, args);
    }
}
