package club.example.cloudservice.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * 模板产品线
 */
@Getter
@AllArgsConstructor
public enum EnumTemplateProductLine {

    MAINTAIN("运营线", 1),
    BUSINESS("商业线", 2);

    // 分类描述
    private String description;

    // 分类编码
    private int code;

    public static EnumTemplateProductLine of(int code) {
        return Stream.of(values())
                .filter(item -> item.code == code)
                .findAny().orElse(MAINTAIN);
    }
}
