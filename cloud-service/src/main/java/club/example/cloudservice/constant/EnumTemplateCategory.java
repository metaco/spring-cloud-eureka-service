package club.example.cloudservice.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;
import java.util.stream.Stream;

/**
 * 模板分类
 */
@Getter
@AllArgsConstructor
public enum EnumTemplateCategory {

    DECREASE("满减", "001"),
    PERCENTAGE("折扣", "002");

    // 分类描述
    private String description;

    // 分类编码
    private String code;

    public static EnumTemplateCategory of(String code) {
        Objects.requireNonNull(code, "could not be null");
        return Stream.of(values())
                .filter(bean -> bean.code.equals(code))
                .findAny()
                .orElse(DECREASE);
    }
}
