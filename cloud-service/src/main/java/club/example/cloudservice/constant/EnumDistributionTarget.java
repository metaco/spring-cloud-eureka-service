package club.example.cloudservice.constant;


import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 *  模板分发目标描述
 */
@Getter
@AllArgsConstructor
public enum EnumDistributionTarget {

    SINGLE("单用户", 1),
    MULTI("多用户", 2);

    // 分类描述
    private String description;

    // 分类编码
    private int code;

    public static EnumDistributionTarget of(int code) {
        return Stream.of(values())
                .filter(item -> item.code == code)
                .findAny().orElse(SINGLE);
    }
}
