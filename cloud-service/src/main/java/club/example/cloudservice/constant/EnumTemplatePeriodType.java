package club.example.cloudservice.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * 模板有效期类别
 */

@Getter
@AllArgsConstructor
public enum EnumTemplatePeriodType {

    REGULAR("固定日期", 1),
    SHIFT("动态日期，从模板领取日计算", 2);

    // 分类描述
    private String description;

    // 分类编码
    private int code;

    public static EnumTemplatePeriodType of(int code) {
        return Stream.of(values())
                .filter(item -> item.code == code)
                .findAny().orElse(SHIFT);
    }
}
