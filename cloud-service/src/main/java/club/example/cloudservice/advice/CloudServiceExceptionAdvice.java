package club.example.cloudservice.advice;

import club.example.cloudservice.exception.CloudServiceBasicException;
import club.example.cloudservice.exception.CloudServiceArgumentException;
import club.example.cloudservice.exception.CloudServiceOperationErrorException;
import club.example.cloudservice.view.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestControllerAdvice
public class CloudServiceExceptionAdvice {

    @ExceptionHandler(value = CloudServiceBasicException.class)
    public JsonResult<String> handleException(HttpServletRequest request,
                                              CloudServiceBasicException ex) {
        if (ex instanceof CloudServiceArgumentException) {
            log.error("serviceArgumentException :", ex);
            return new JsonResult<>(-10001, "参数错误异常");
        }

        if (ex instanceof CloudServiceOperationErrorException) {
            log.error("serviceOperationException : ", ex);
            return new JsonResult<>(-10009, "操作失败.");
        }

        log.error("error exception :", ex);
        return new JsonResult<>(-100010, ex.getMessage());
    }
}
