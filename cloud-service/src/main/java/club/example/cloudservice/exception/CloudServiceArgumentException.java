package club.example.cloudservice.exception;

public class CloudServiceArgumentException extends CloudServiceBasicException {

    public CloudServiceArgumentException(String message) {
        super(message);
    }

    public CloudServiceArgumentException(String message, int serviceErrorCode) {
        super(message, serviceErrorCode);
    }
}
