package club.example.cloudservice.exception;


public class CloudServiceOperationErrorException extends CloudServiceBasicException {

    public CloudServiceOperationErrorException(String message) {
        super(message);
    }

    public CloudServiceOperationErrorException(String message, int serviceErrorCode) {
        super(message, serviceErrorCode);
    }
}
