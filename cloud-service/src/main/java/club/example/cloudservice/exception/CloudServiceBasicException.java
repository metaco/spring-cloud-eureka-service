package club.example.cloudservice.exception;

/**
 *
 */
public class CloudServiceBasicException extends RuntimeException {

    public CloudServiceBasicException(String message) {
        super(message);
    }

    public CloudServiceBasicException(String message, int serviceErrorCode) {
        super(String.format("service error(error_code:%s; message: %s)", serviceErrorCode, message));
    }

    public CloudServiceBasicException(String message, Throwable cause) {
        super(message, cause);
    }
}
